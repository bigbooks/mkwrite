
---

## 4.3 Hexo的使用

Hexo使用nodejs实现，也是一款基于markdown的静态站点生成工具，Hexo由台湾的工程师贡献，在中文社区使用较为广泛，Hexo可以视为Jekyll的一个变种，目的是为了避免Jekyll基于ruby而不够轻量（由于亲缘关系，Jekyll是github pages官方支持的实现方式），由于内容的组织上以时间线而非内容逻辑组织，因此较为适合个人博客的编写。

### 4.3.1 hexo的安装

hexo也是基于nodejs，因此需要先安装nodejs，然后在进行安装，安装过程中需要联网解决依赖问题，如无网络请查看离线安装方式。

```
npm install hexo
hexo init new_site
cd new_site
hexo generate
hexo serve
```

### 4.3.2 Hexo工程的版本管理

Hexo的版本管理和mkdocs一样，如果直推送会产生版本库混乱的问题，策略同mkdocs.

