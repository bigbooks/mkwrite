
---

## 4.1 Gitbook的组成

Gitbook生态包含Gitbook Editor、Gitbook Serve、Gitbook CLI，分别用于文档编辑、静态站点预览、文档操作（转换成各种格式），Gitbook主要用于编辑书籍而生，但也可以用于编辑普通文档，如果需要进行博客协作及专门的项目文档，可以在完成文字编辑后适用其它专门的站点生成工具进行生成。

## 4.2 Gitbook Editor

Gitbook Editor是gitbook的编辑器，底层适用markdown+git实现，但是所支持的语法比标准的markdown小，但是可以切换到普通的markdown方式编辑，如果语法范围超出Gitbook Editor的表达范围，则无法切换回Gitbook Editor的标准界面，只能继续适用markdown方式编写，但对于生成结果来说没有影响，另外，Gitbook Editor还有很多高级的定制手段可以获得更多的效果。

比较适合非专业人员使用的Markdown编辑器，所见即所得，能够有效的组织内容及目录结构，同时结合配套的命令行工具，能够实现静态站点预览、电子书输出等。

### 4.2.1 Gitbook Editor 安装

图形化安装界面，安装较为简单，不进行详细介绍，具体参照[Gitbook](https://www.gitbook.com/)官方文档。

### 4.2.2 Gitbook 注册及登陆

由于Gitbook网站在国外，注册比较困难，本书未使用Gitbook官方的账号，而是采用离线编辑+Github方式使用，参照后续的内容。

### 4.2.3 Gitbook Editor的离线编辑

* 进入离线编辑

Gitbook Editor编辑器鼓励用户适用[Gitbook](https://www.gitbook.com/)官方账号登陆后编辑，因此，离线编辑功能比较隐蔽，启动Gitbook Editor后进入离线编辑的入口如下：

![](/assets/GitbookStartup_UI.png)

* 创建图书

图书的默认保存路径

```
C:\Users\ssd\GitBook\Library\Import
```

_**注意，Gitbook Editor目前尚不支持中文路径及中文文件名！**_

* 编辑内容

**正常编辑：**

正常编辑可以适用Gitbook Editor的工具栏进行设置格式，包括常用的字体加粗、斜体、超链接、插入图片、章节层级、数学符号等。

Gitbook的书籍工程目录结构：

```
├───.git
├───.gitignore
├───assets
│       GitbookMarkdown.png
│       GitbookStartup_UI.png
│       gitflow_from_baidu.jpg
│       git_flow.png
│       git司令官和副官工作流.png
│       git基础工作流.png
│       git集成者工作流.png
│       pycharm_markdown.png
│
├───chapter1.md
├───chapter2.md
├───chapter3.md
├───chapter3
├───chapter4.md
├───chapter5.md
├───chapter5
│       gitbook.md
│       hexo.md
│       mkdocs.md
├───README.md
├───SUMMARY.md
```

.git/.gitignore是git仓库所适用的文件路径

assets是Gitbook Editor默认的保存图片的位置，这个位置可以修改或者增加，目录和文件的管理最好全部适用Gitbook Editor右边栏上的功能进行操作。

README.md 书籍的前言介绍内容。

SUMMARY.md是Gitbook工程的目录表述文件。

**markdown编辑模式：**

从右下角进入，如果所用语法超出Gitbook Editor的标准格式，则无法切换回Gitbook Editor的标准编辑界面。![](/assets/EditorWithMarkdown.png)

* 保存及同步

Gitbook Editor中的保存实际是调用git的commit操作，而同步操作实际是调用git的push操作，不过在使用过程中发现Gitbook Editor的同步只支持https协议，而配置好免密码登陆后，git命令行可以执行push，而Gitbook Editor中无法执行同步，目前原因未明：

![](/assets/GitbookSaveSync.png)

## 4.3 Gitbook Server

Gitbook命令行工具包含Gitbook Server和Gitbook Cli，具体参见[Github](https://github.com/GitbookIO/gitbook)。

### 4.3.1 Gitbook Server的安装

Gitbook Server基于nodejs运行，所以首先需要安装nodejs，关于nodejs的内容请参考[nodejs](https://nodejs.org/en/)相关文档。

在nodejs下安装gitbook（包含gitbook server和gitbook cli）

```
npm install gitbook -g
```

### 4.3.2 Gitbook Server的启动

```
gitbook init .        #创建一本书，也可以在Gitbook Editor中执行创建
gitbook serve         #启动预览服务，可以在浏览器看到效果

Live reload server started on port: 35729
Press CTRL+C to quit ...

info: 7 plugins are installed
info: loading plugin "livereload"... OK
info: loading plugin "highlight"... OK
info: loading plugin "search"... OK
info: loading plugin "lunr"... OK
info: loading plugin "sharing"... OK
info: loading plugin "fontsettings"... OK
info: loading plugin "theme-default"... OK
info: found 8 pages
info: found 13 asset files
info: >> generation finished with success in 1.9s !

Starting server ...
Serving book on http://localhost:4000
```

gitbook serve在执行过程中如果适用Gitbook Editor编辑会导致gitbook serve中断服务，需要重新启动。

## 4.4 Gitbook CLI

### 4.4.1 Gitbook CLI的安装

```
npm install -g gitbook-cli
```

在windows命令行下，gitbook命令会输出乱码，dos命令行窗口出现乱码时，尝试变更编码方式  
。

在当前命令行窗口执行：

```
chcp 65001       #改成UTF8
```

或：

```
chcp  936        #改成GBK
```

### 4.4.2 Calibre的安装与配置

gitbook生成PDF需要依赖于外部工具[Calibre](https://calibre-ebook.com/)，详细参见[Calibre](https://calibre-ebook.com/)的站点介绍。

Calibre安装需要配置环境变量，以便在命令行能够调用ebook-convert.exe

例如：

```
set PATH=C:\tools\Calibre Portable\Calibre;%PATH%
```

配置后可以在命令行测试

```
C:\>ebook-convert --version

ebook-convert.exe (calibre 3.22.1)
Created by: Kovid Goyal <kovid@kovidgoyal.net>
```

### 4.4.3 用Gitbook输出结果

当完成书籍编辑后，可以生成PDF文件（在书籍工程目录下）

```
gitbook pdf .   book_name.pdf         #生成PDF文件
```

或：

```
gitbook build --format website    site  #在site目录下生成静态网站
```

### 4.4.4 Gitbook的插件

安装插件：

```
gitbook install plugin_name
```

插件配置文件为`book.json`

关于插件的详细适用请查阅相关资料文档。

## 4.5 Gitbook 与Git结合

本文即使用Gitbook编辑，Gitbook和Git紧密结合，使用Git进行版本管理，可以连接Gitbook自己的官方服务器，也可以连接第三方的Git版本库，但是目前连接远程资料库在使用上仍然存在一些问题，不过这个可以使用git的命令行进行版本推送和拉取进行弥补。

另外，在插入图片后，输入过程中会出现频繁刷屏的问题，可以最后再插入图片，规避该问题。

使用其它编辑工具编辑Gitbook工程的文件在未commit前会导致Git Editor无法识别，因此所有的操作最好都在Gitbook Editor中进行，或者打开Gitbook Editor前进行commit。

如果未使用Gitbook的官方网站，也可以使用git hook的持续集成方式自动发布编辑后的文档到web站点。关于如果结合git进行持续集成请参与git的相关文档。

