
---

## 4.2 Mkdocs使用介绍

mkdocs生成的静态站点在逻辑上和gitbook基本相当，都是以内容为中心，比较适于编写书籍及项目文档，进行知识整理。

### 4.2.1 安装mkdocs

* 安装python

mkdocs使用python实现，因此首先需要安装python，本文推荐[anaconda](https://www.anaconda.com/)作为python的解释器，python环境安装请查阅相关文档，为了隔离影响，安装完成后建议适用conda或virtualenv等工具为mkdocs创建专门的虚拟环境，这是一个适用python的好习惯。

* 使用pip安装mkdocs

安装需要连接网络，安装命令如下：

```bash
pip install mkdocs
```

_**pip安装需要连接网络，如果无法直接连接网络请自行搭建内部pip站点或者解决依赖问题。**_

### 4.2.2 创建站点

* `mkdocs new [dir-name]` - 新建一个站点工程.

* `mkdocs serve` - 启动http服务，在工程项目目录下执行，不同于hexo等工具，这个命令没有简化形式，必须完全输入.

* `mkdocs build` - 显式执行站点的构建，将markdown文档转换成html站点，如果上一步http服务已经启动，每次保存将自动构建.

* `mkdocs help` - 帮助信息.

### 4.2.3 mkdocs的结构

mkdocs工程的目录结构

```
(mkdocs) C:myid.github.io>tree /F .
文件夹 PATH 列表
卷序列号为 00000028 F0C7:49B5
C:\MYID.GITHUB.IO
│  .gitignore
│──.git
│  │  ...
│──site
│  │  index.html
|
│  mkdocs.yml
├─docs
│  │  index.md
│  │  white_paper.md
│  │
│  ├─about
│  │  │  about.md
│  │  │
│  │  └─images
│  │          weighted_graph.png
```

.git、.gitignore为git的相关目录，site为mkdocs的静态站点默认输出路径，docs为源文件，mkdocs.yml为工程的配置文件，存放目录结构及站点基本信息，详细参见mkdocs的相关文档。

### 4.2.4 mkdocs工程的版本跟踪

mkdocs提供了直接推送到远程仓库的命令：

```
mkdocs gh-deploy      #github deploy or other remote git server
```

但是适用该命令会将源文件从仓库删除，如果需要保存仓库文件还需要再次使用git单独提交源文件（\*.md），繁琐而且容易使仓库变得混乱，因此可以采用如下策略：

```
cd .
git add remote origin git@github.com/mypath/gitbook_source     #添加远程仓库地址
git push --set-upstream origin                                 #推送源文件到远程仓库，不要和静态网站同一个库
mkdocs build -c                                                #清理创建
cd site                                                        #转到静态网站目录
git init                                                       #初始化git仓库
git add remote origin git@github.com/mypath/gitbook_target     #添加远程仓库地址
git push --set-upstream origin                                 #将静态网站推送到远程仓库
```

即专门为输出的静态站点site目录创建一个git仓库进行单独管理，而不再使用mkdocs gh-deploy命令。另外需要注意的就是工程配置文件mkdocs.yml采用YAML语法，对缩进敏感，需要注意格式。

