# 第三章 Markdown编辑器

---

本部分列举几个常用的Markdown编辑器，供后续使用时选取。

## 3.1 用notepad进行markdown编写

最简单的文本编辑器就可以用来编写mardown文件，但是有预览功能或者语法提示功能的IDE可以更好的提升工作效率。

## 3.2 用IDE\(Pycharm\)进行markdown编写

Pycharm中可以安装markdown编辑器插件，支持语法高亮显示和分屏预览，在连接网络的情况下只有打开名称为\*.md的文件时就可以自动下载并安装该插件。

![](/assets/pycharm_markdown.png)

## 3.3 用Gitbook Editor进行markdown编写

由于Gitbook网站登陆存在问题，需要使用离线编辑模式，Gitbook Editor的离线编辑入口很隐蔽：

![](/assets/GitbookStartup_UI.png)

![](/assets/GitbookMarkdown.png)

## 3.4 用Ms Word（writage）进行文档转换

word安装插件writage可以编辑markdown文件，也可以将现有的word文档转换成mardown格式，但存在兼容性问题和有些格式不支持及图片找不到的问题，需要手工继续编辑。

由于markdown在文档管理上的巨大优势，建议一旦生成markdown格式的文档后就不要再继续维护原来的word文档。

该工具仅用于将原有word迁移到markdown，不作为真正的markdown编辑器使用。

